<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserBlockTable extends Migration
{
    const TABLE = 'USER_BLOCKED';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE, function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('blocker');
            $table->unsignedInteger('blockee');

            $table->foreign('blocker')->references('id')->on('users');
            $table->foreign('blockee')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE);
    }
}
