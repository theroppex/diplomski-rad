<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AtlterPostsTableRemoveTopicIdColumn extends Migration
{
    const TABLE = 'posts';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::TABLE, function (Blueprint $table){
            $table->dropForeign('posts_topic_id_foreign');
            $table->dropColumn('topic_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE, function (Blueprint $table){
            $table->unsignedInteger('topic_id')->after('data');
            $table->foreign('topic_id')->references('id')->on('topics');
        });
    }
}
