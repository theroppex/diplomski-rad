<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $time = \Carbon\Carbon::now()->toDateTimeString();
        $defaultTopics = [
            [
                'title' => 'Java',
                'created_at' => $time,
            ],
            [
                'title' => 'C#',
                'created_at' => $time,
            ],
            [
                'title' => 'C',
                'created_at' => $time,
            ],
            [
                'title' => 'C++',
                'created_at' => $time,
            ],
            [
                'title' => 'php',
                'created_at' => $time,
            ],
            [
                'title' => 'Web',
                'created_at' => $time,
            ],
            [
                'title' => 'Data',
                'created_at' => $time,
            ],
        ];

        \App\Topic::insert($defaultTopics);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
