<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'topic'])->group(function (){
    /**
     * USERS ROUTES
     */
    Route::get('/users', 'UsersController@index');
    Route::get('/users/filter/', 'UsersController@filter');
    Route::get('/users/{user}/profile', 'UsersController@getProfile');
    Route::put('/users/{user}/profile', 'UsersController@editProfile');
    Route::get('/users/{user}/password', 'UsersController@getChangePassword');
    Route::post('/users/{user}/password', 'UsersController@changePassword');
    Route::post('/users/{user}/avatar', 'UsersController@changeProfilePicture');
    Route::post('/users/{user}/additional_info', 'UsersController@setAdditionalInfo');
    Route::get('/users/{user}/block', 'UsersController@blockUser');
    Route::get  ('/users/{user}/unblock', 'UsersController@unblockUser');
    Route::get('/users/{user}/follow', 'UsersController@followUser');
    Route::get('/users/{user}/unfollow', 'UsersController@unfollowUser');
    Route::get('/users/personalinfo', 'UsersController@getPersonalInfo');

    /**
     * CHAT ROUTES
     */
    Route::get('/chat', 'ChatController@index');
    Route::get('/chat/messages', 'ChatController@readMessages');
    Route::post('/chat/messages', 'ChatController@createMessage');

    /**
     * NEWS ROUTES
     */
    Route::get('/news', 'NewsPostsController@index');
    Route::post('/news', 'NewsPostsController@create');
    Route::get('/news/{newsPost}/delete', 'NewsPostsController@delete');
    Route::get('/news/user/{user}', 'NewsPostsController@getForUser');

    /**
     * COURSES ROUTES
     */
    Route::get('/courses', 'CoursesController@index');
    Route::post('/courses', 'CoursesController@create');
    Route::get('/courses/{course}', 'CoursesController@viewCourse');
    Route::get('/courses/{course}/follow', 'CoursesController@followCourse');
    Route::get('/courses/{course}/unfollow', 'CoursesController@unfollowCourse');
    Route::get('/courses/{course}/create/text', 'CoursesController@getCreateText');
    Route::post('/courses/{course}/create/text', 'CoursesController@postCreateText');
    Route::post('/courses/{course}/create/video', 'CoursesController@createVideoPost');
    Route::get('/posts/text/{post}', 'CoursesController@getTextPost');
    Route::get('/posts/video/{post}', 'CoursesController@getVideoPost');
    Route::post('/posts/{post}/comments', 'CoursesController@createComment');
    Route::post('/courses/search', 'CoursesController@search');
    Route::get('/comments/delete/{comment}', 'CoursesController@deleteComment');
    Route::get('/posts/delete/{post}', 'CoursesController@deletePost');
    Route::get('/courses/delete/{course}', 'CoursesController@deleteCourse');
    Route::get('/courses/topic/{topic}', 'CoursesController@getByTopic');
    Route::get('/posts/{post}/complete', 'CoursesController@completePost');

    /**
     * MESSAGES ROUTES
     */
    Route::get('/messages', 'MessagesController@get');
    Route::get('/messages/{user}', 'MessagesController@getForRecipient');
    Route::post('/messages/{user}', 'MessagesController@create');
    Route::get('/messages/delete/{privateMessages}', 'MessagesController@delete');
});

Route::middleware(['auth'])->group(function (){
    Route::get('/topic/favourite', 'TopicsController@getFavouriteTopicPage');
    Route::post('/topic/favourite', 'TopicsController@saveFavouriteTopic');
});