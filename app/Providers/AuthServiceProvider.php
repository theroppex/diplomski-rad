<?php

namespace App\Providers;

use App\Authorization\Gates\UserGate;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerUserGates();
    }

    /**
     * Registers user gates.
     */
    private function registerUserGates()
    {
        $userGate = UserGate::class;
        Gate::define('view_edit_user_profile_page', "$userGate@canViewEditUserProfilePage");
        Gate::define('edit_user_profile', "$userGate@canEditUserProfile");
        Gate::define('can_change_password', "$userGate@canChangePassword");
        Gate::define('can_change_profile_picture', "$userGate@canChangeProfilePicture");
        Gate::define('can_block_user', "$userGate@canBlockUser");
        Gate::define('can_unblock_user', "$userGate@canUnblockUser");
        Gate::define('can_follow_user', "$userGate@canFollowUser");
        Gate::define('can_unfollow_user', "$userGate@canUnfollowUser");
        Gate::define('can_access_chat', "$userGate@canAccessChat");
        Gate::define('can_delete_news_post', "$userGate@canDeleteNewsPost");
        Gate::define('can_follow_course', "$userGate@canFollowCourse");
        Gate::define('can_unfollow_course', "$userGate@canUnfollowCourse");
        Gate::define('can_delete_comment', "$userGate@canDeleteComment");
        Gate::define('can_delete_post', "$userGate@canDeletePost");
        Gate::define('can_delete_course', "$userGate@canDeleteCourse");
        Gate::define('can_complete_post', "$userGate@canCompletePost");
    }
}
