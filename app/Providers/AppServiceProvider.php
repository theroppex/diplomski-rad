<?php

namespace App\Providers;

use App\Contracts\Repositories\UserMetaRepository;
use App\Contracts\Repositories\UserRepository;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind(UserMetaRepository::class, \App\Repositories\UserMetaRepository::class);
        app()->bind(UserRepository::class, \App\Repositories\UserRepository::class);
    }
}
