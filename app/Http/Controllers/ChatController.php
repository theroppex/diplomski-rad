<?php

namespace App\Http\Controllers;

use App\ChatMessage;
use App\Events\MessageCreatedEvent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ChatController extends Controller
{
    /**
     * ChatController constructor.
     */
    public function __construct()
    {
        $this->middleware('chat_access');
    }

    /**
     * Retrieves chat page.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('chat.chat');
    }

    /**
     * Retrieves recent chat messages
     */
    public function readMessages()
    {
        $timeCutoff = Carbon::now()->subHour(1)->toDateTimeString();

        return ChatMessage::where('created_at', '>=', $timeCutoff)
            ->orderBy('id', 'desc')
            ->take(20)
            ->get()
            ->reverse()
            ->values()
            ->all();
    }

    /**
     * Creates message.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function createMessage(Request $request)
    {
        $data = $request->all(['message', 'username']);

        if ($this->messageValidator($data)->fails()) {
            return response('', 400);
        }

        $message = ChatMessage::create($data);

        event(new MessageCreatedEvent($message));

        return response('', 201);
    }

    /**
     * Generates message validator.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function messageValidator(array $data)
    {
        return Validator::make($data, [
            'message' => 'required|string|max:140',
            'username' => 'required|string|max:60',
        ]);
    }
}
