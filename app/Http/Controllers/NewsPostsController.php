<?php

namespace App\Http\Controllers;

use App\NewsPost;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class NewsPostsController extends Controller
{
    /**
     * Retrieves news page.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        /** @var User $user */
        $user = Auth::user();
        $followedUserIds = $user->followedUsers()->pluck('followed');

        $newsPosts = NewsPost::whereIn('author', $followedUserIds)
            ->orWhere('author', $user->id)
            ->orderBy('created_at', 'desc')
            ->paginate(6);

        return view('news.news', ['posts' => $newsPosts]);
    }

    /**
     * Retrieves news posts for a specific user.
     *
     * @param User $user
     * @return \Illuminate\View\View
     */
    public function getForUser(User $user)
    {
        $newsPosts = NewsPost::where('author', $user->id)->orderBy('created_at', 'desc')->paginate(6);

        return view('news.news', ['posts' => $newsPosts]);
    }

    /**
     * Creates news post.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        $data = $request->all(['title', 'content']);
        $this->newsPostsValidator($data)->validate();

        $post = new NewsPost($data);
        $post->author = Auth::user()->id;
        $post->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Your post has been saved!');

        return redirect()->back();
    }

    /**
     * Deletes news post.
     *
     * @param NewsPost $newsPost
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Exception
     */
    public function delete(NewsPost $newsPost)
    {
        if (Gate::allows('can_delete_news_post', $newsPost)) {
            $newsPost->delete();
            Session::flash('status', 'success');
            Session::flash('message', 'Your post has been deleted!');
        }

        return redirect()->back();
    }

    /**
     * Generates news post validator.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function newsPostsValidator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|string|max:60',
            'content' => 'required|string|max:140',
        ]);
    }
}
