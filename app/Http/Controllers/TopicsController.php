<?php

namespace App\Http\Controllers;

use App\Topic;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TopicsController extends Controller
{
    /**
     * Retrieves page where user can choose favourite topic.
     *
     * @return \Illuminate\View\View
     */
    public function getFavouriteTopicPage()
    {
        $topics = Topic::all();

        return view('topics.favourite', ['topics' => $topics]);
    }

    /**
     * Saves favourite topic.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveFavouriteTopic(Request $request)
    {
        $defaultTopic = (int) $request->get('topic');
        /** @var User $user */
        $user = Auth::user();

        if (Topic::where('id', $defaultTopic)->exists()) {
            $user->favourite_topic = $defaultTopic;
            $user->save();
        }

        if ($request->get('fromProfile', false)) {
            return redirect("/users/$user->id/profile");
        }

        return redirect()->intended("/home");
    }
}
