<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Course;
use App\Post;
use App\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CoursesController extends Controller
{
    /**
     * Retrieves available courses for a specific user.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();
        $topics = Topic::all();
        $courses = Course::paginate(10);

        return view('courses.list', ['user' => $user, 'topics' => $topics, 'courses' => $courses]);
    }

    /**
     * Creates course.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        $data = $request->all();

        if ($this->coursesValidator($data)->fails()) {
            Session::flash('status', 'danger');
            Session::flash('message', 'Failed to save course!');

            return redirect()->back();
        }

        $course = new Course();
        $course->title = $data['title'];
        $course->description = $data['description'];
        $course->topic_id = (int) $data['topic'];
        $course->user_id = Auth::user()->id;
        $course->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Course saved successfully!');

        return redirect()->back();
    }

    /**
     * Follows course.
     *
     * @param Course $course
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function followCourse(Course $course)
    {
        if (Gate::allows('can_follow_course', $course)) {
            Auth::user()->followedCourses()->attach($course);
        }

        return redirect()->back();
    }

    /**
     * Follows course.
     *
     * @param Course $course
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function unfollowCourse(Course $course)
    {
        if (Gate::allows('can_unfollow_course', $course)) {
            Auth::user()->followedCourses()->detach($course);
        }

        return redirect()->back();
    }

    /**
     * Displays specific course.
     *
     * @param Course $course
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewCourse(Course $course) {
        return view('courses.course', ['course' => $course]);
    }

    /**
     * Returns form for creating text post.
     *
     * @param Course $course
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreateText(Course $course) {
        return view('courses.create.text', ['course' => $course]);
    }

    /**
     * Creates textual post.
     *
     * @param Course $course
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreateText(Course $course, Request $request) {
        $data['title'] = $request->get('title');
        $data['description'] = $request->get('description');
        $data['body'] = $request->get('body');

        if ($this->getTextPostValidator($data)->fails()) {
            Session::flash('status', 'danger');
            Session::flash('message', 'Failed to create text post');

            return redirect()->back()->withInput($request->all());
        }

        $post = new Post();
        $post->title = $data['title'];
        $post->description = $data['description'];
        $post->type = 'text';
        $post->data = $data['body'];
        $post->user_id = Auth::user()->id;
        $post->course_id = $course->id;
        $post->save();

        Session::flash('status', 'success');
        Session::flash('message', 'Successfully created textual post.');

        return redirect('/courses/' . $course->id);
    }

    /**
     * Retrieves text post.
     *
     * @param Post $post
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTextPost(Post $post)
    {
        return view('posts.text', ['post' => $post]);
    }

    /**
     * Retrieves video post.
     *
     * @param Post $post
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getVideoPost(Post $post)
    {
        return view('posts.video', ['post' => $post]);
    }

    /**
     * Creates comment.
     *
     * @param Post $post
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createComment(Post $post, Request $request) {
        $data['body'] = $request->get('body');
        if (!$this->getCommentValidator($data)->fails()) {
            $comment = new Comment();
            $comment->comment = $data['body'];
            $comment->user_id = Auth::user()->id;
            $comment->post_id = $post->id;
            $comment->save();
        }

        return redirect()->back();
    }

    /**
     * Performs search for specific course.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $user = Auth::user();
        $topics = Topic::all();

        $search = $request->get('search');

        if ($search) {
            $courses = Course::search($search)->paginate(10);
        } else {
            $courses = Course::paginate(10);
        }

        return view('courses.list', ['user' => $user, 'topics' => $topics, 'courses' => $courses]);
    }

    /**
     * Retrieves courses by topic.
     *
     * @param Topic $topic
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getByTopic(Topic $topic) {
        $courses = $topic->courses()->paginate(10);
        $user = Auth::user();
        $topics = Topic::all();

        return view('courses.list', ['user' => $user, 'topics' => $topics, 'courses' => $courses]);
    }

    /**
     * Deletes comment.
     *
     * @param Comment $comment
     *
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Exception
     */
    public function deleteComment(Comment $comment)
    {
        if (Gate::allows('can_delete_comment', $comment)) {
            $comment->delete();
        }

        return redirect()->back();
    }

    /**
     * Deletes post.
     *
     * @param Post $post
     *
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Exception
     */
    public function deletePost(Post $post)
    {
        if (Gate::allows('can_delete_post', $post)) {
            $post->delete();
        }

        return redirect()->back();
    }

    /**
     * Deletes course.
     *
     * @param Course $course
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Exception
     */
    public function deleteCourse(Course $course)
    {
        if (Gate::allows('can_delete_course', $course)) {
            $course->delete();
        }

        return redirect()->back();
    }

    /**
     * Creates video post.
     *
     * @param Course $course
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createVideoPost(Course $course, Request $request)
    {
        $data['video'] = $request->file('video');
        $data['title'] = $request->get('title');
        $data['description'] = $request->get('description');

        if (!$this->getVideoPostValidator($data)->fails()) {
            $post = new Post();
            $post->title = $data['title'];
            $post->description = $data['description'];
            $post->data = $data['video']->store('videos');
            $post->type = 'video';
            $post->user_id = Auth::user()->id;
            $post->course_id = $course->id;
            $post->save();

            Session::flash('status', 'success');
            Session::flash('message', 'Successfully created video post.');
        }

        return redirect()->back();
    }

    /**
     * Completes post.
     *
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function completePost(Post $post)
    {
        if (Gate::allows('can_complete_post', $post)) {
            Auth::user()->completedPosts()->attach($post);
        }

        return redirect()->back();
    }

    /**
     * Generates courses validator.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function coursesValidator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|string|max:60',
            'description' => 'required|string|max:140',
            'topic' => 'required'
        ]);
    }

    /**
     * Retrieves text post validator.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getTextPostValidator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|string|max:60',
            'description' => 'required|string|max:140',
            'body' => 'required|string',
        ]);
    }

    /**
     * Generates video validator.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getVideoPostValidator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|string|max:60',
            'description' => 'required|string|max:140',
        ]);
    }

    /**
     * Retrieves comment validator.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getCommentValidator(array $data)
    {
        return Validator::make($data, [
            'body' => 'required|string|max:360',
        ]);
    }
}
