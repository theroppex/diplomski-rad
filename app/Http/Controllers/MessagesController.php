<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\UserRepository;
use App\DeletedMessages;
use App\PrivateMessages;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MessagesController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $usersRepository;

    /**
     * MessagesController constructor.
     * @param UserRepository $usersRepository
     */
    public function __construct(UserRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    /**
     * Retrieves messages page.
     *
     * @return \Illuminate\View\View
     */
    public function get()
    {
        return view('messages.messages', $this->getViewData());
    }

    /**
     * Retrieves messages for specific recipient.
     *
     * @param User $user
     * @return \Illuminate\View\View
     */
    public function getForRecipient(User $user)
    {
        return view('messages.messages', $this->getViewData($user));
    }

    /**
     * Creates message.
     *
     * @param User $user
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(User $user, Request $request)
    {
        $data = [
            'message' => $request->get('message')
        ];

        $this->getMessagesValidator($data)->validate();

        /** @var User $currentUser */
        $currentUser = Auth::user();

        if ($user->blockedUsers()->where('blockee', $currentUser->id)->exists()) {
            return redirect()->back();
        }

        $message = new PrivateMessages($data);
        $message->user_id = $currentUser->id;
        $message->recipient_id = $user->id;
        $message->save();

        return redirect()->back();
    }

    /**
     * Deletes message for current user.
     *
     * @param PrivateMessages $privateMessages
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(PrivateMessages $privateMessages) {
        /** @var User $currentUser */
        $currentUser = Auth::user();

        $deleteRecord = new DeletedMessages();
        $deleteRecord->user_id = $currentUser->id;
        $deleteRecord->message_id = $privateMessages->id;
        $deleteRecord->save();

        return redirect()->back();
    }

    /**
     * Retrieves view data.
     *
     * @param User|null $selectedUser
     * @return array
     */
    protected function getViewData(User $selectedUser = null): array
    {
        $result['selectedUserId'] = $selectedUser ? $selectedUser->id : 0;
        $result['selectedUserName'] = $selectedUser ? $selectedUser->name : '';

        /** @var User $currentUser */
        $currentUser = Auth::user();
        $result['recipients'] = $this->usersRepository->getRecipients($currentUser);

        if ($selectedUser) {
            $group = [$currentUser->id, $selectedUser->id];
            $deletedMessages = DeletedMessages::where('user_id', $currentUser->id)->get()->pluck('id');
            $result['messages'] = PrivateMessages::whereNotIn('id', $deletedMessages)
                ->whereIn('user_id', $group)
                ->whereIn('recipient_id', $group)->get();

        } else {
            $result['messages'] = Collection::make();
        }

        return $result;
    }

    /**
     * Validates message.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getMessagesValidator(array $data)
    {
        return Validator::make($data, [
            'message' => 'required|string|max:191'
        ]);
    }
}
