<?php /** @noinspection PhpComposerExtensionStubsInspection */

namespace App\Http\Controllers;

use App\ChatMessage;
use App\Contracts\Repositories\UserMetaRepository;
use App\NewsPost;
use App\PrivateMessages;
use App\Topic;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class UsersController extends Controller
{
    const PAGE_SIZE = 10;

    const EDITABLE_PROFILE_INFORMATION = [
        'name',
        'last_name',
    ];

    /** @var UserMetaRepository */
    protected $userMetaRepository;

    /**
     * UsersController constructor.
     * @param UserMetaRepository $userMetaRepository
     */
    public function __construct(UserMetaRepository $userMetaRepository)
    {
        $this->userMetaRepository = $userMetaRepository;
    }

    /**
     * Returns default users view.
     *
     * @GET('/users/')
     *
     * @return View
     */
    public function index(): View
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $users = User::paginate(self::PAGE_SIZE);

        return view('users.users', ['users' => $users]);
    }

    /**
     * Filters users by search criteria.
     *
     * @GET('/users/filter/?q=some_query').
     *
     * @param Request $request
     * @return View
     */
    public function filter(Request $request): View
    {
        $searchParameter = $request->get('q');

        if ($searchParameter === null) {
            $users = User::paginate(10);
        } else {
            $users = User::search($searchParameter)->paginate(10);
        }

        return view('users.snippets.users', ['users' => $users]);
    }

    /**
     * Retrieves users profile page.
     *
     * @GET('/user/{id}/profile')
     *
     * @param User $user
     * @return View
     */
    public function getProfile(User $user)
    {
        if (Gate::denies('view_edit_user_profile_page', $user)) {
            return redirect()->back();
        }

        $meta = $this->userMetaRepository->getUserMetaData($user);
        $newsPosts = NewsPost::where('author', $user->id)->paginate(10);
        $topics = Topic::all();

        return view('users.profile', ['user' => $user, 'meta' => $meta, 'news' => $newsPosts, 'topics' => $topics,]);
    }

    /**
     * Updates user profile.
     *
     * @PUT('/users/{user}/profile')
     *
     * @param Request $request
     * @param User $user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editProfile(Request $request, User $user)
    {
        if (Gate::denies('edit_user_profile', $user)) {
            return redirect()->back();
        }

        $data = array_intersect_key($request->all(), array_flip(self::EDITABLE_PROFILE_INFORMATION));
        $this->profileValidator($data)->validate();

        $user->update($data);

        return redirect()->back();
    }

    /**
     * Saves additional information.
     *
     * @param Request $request
     * @param User $user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setAdditionalInfo(Request $request, User $user)
    {
        if (Gate::denies('edit_user_profile', $user)) {
            return redirect()->back();
        }

        $data = $request->all(\App\Repositories\UserMetaRepository::ALLOWED_KEYS);

        $this->additionalInfoValidator($data)->validate();
        $this->userMetaRepository->setUserMetaData($user, $data);

        return redirect()->back();
    }

    /**
     * Retrieves change user password form.
     *
     * @param User $user
     *
     * @return \Illuminate\Http\RedirectResponse | View
     */
    public function getChangePassword(User $user)
    {
        if (Gate::denies('can_change_password', $user)) {
            return redirect()->back();
        }

        return view('users.changePasswordForm');
    }

    /**
     * Changes user password.
     *
     * @param Request $request
     * @param User $user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePassword(Request $request, User $user)
    {
        if (Gate::denies('can_change_password', $user)) {
            return redirect()->back();
        }

        $data = $request->all();

        $this->changePasswordValidator($data)->validate();
        if (!Hash::check($data['old_password'], $user->password)) {
            return redirect()->back()->withErrors(['old_password' => 'Old password is incorrect!']);
        }

        $user->update(['password' => Hash::make($data['new_password'])]);

        Session::flash('status', 'success');
        Session::flash('message', 'Password has been changed!');

        return redirect()->back();
    }

    /**
     * Blocks user.
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function blockUser(User $user)
    {
        if (Gate::allows('can_block_user', $user)) {
            Auth::user()->blockedUsers()->attach($user);
        }

        return redirect()->back();
    }

    /**
     * Unblocks user.
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function unblockUser(User $user)
    {
        if (Gate::allows('can_unblock_user', $user)) {
            Auth::user()->blockedUsers()->detach($user);
        }

        return redirect()->back();
    }

    /**
     * Follows user.
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function followUser(User $user)
    {
        if (Gate::allows('can_follow_user', $user)) {
            Auth::user()->followedUsers()->attach($user);
        }

        return redirect()->back();
    }

    /**
     * Unfollows user.
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function unfollowUser(User $user)
    {
        if (Gate::allows('can_unfollow_user', $user)) {
            Auth::user()->followedUsers()->detach($user);
        }

        return redirect()->back();
    }

    /**
     * Changes users profile picture.
     *
     * @param Request $request
     * @param User $user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeProfilePicture(Request $request, User $user)
    {
        if (Gate::denies('can_change_profile_picture', $user)) {
            return redirect()->back();
        }

        $this->profilePictureValidator(['avatar' => $request->file('avatar')])->validate();
        $filePath = $request->file('avatar')->store('avatars');
        $user->update(['avatar_url' => $filePath]);
        Session::flash('status', 'success');
        Session::flash('message', 'Profile picture changed successfully!');

        return redirect()->back();
    }

    /**
     * Retrieves personal info as a zip archive.
     */
    public function getPersonalInfo()
    {
        $file = tempnam(sys_get_temp_dir(), 'pi');
        $zipArchive = new \ZipArchive();

        $zipArchive->open($file, \ZipArchive::CREATE);

        /** @var User $currentUser */
        $currentUser = Auth::user();
        $zipArchive->addFromString('user.txt.', $currentUser->toJson());

        $metaData = $this->userMetaRepository->getUserMetaData($currentUser);
        $zipArchive->addFromString('meta-data.txt', json_encode($metaData));

        $messages = PrivateMessages::where('user_id', $currentUser->id)->get();
        $zipArchive->addFromString('messages.txt', $messages->toJson());

        $chatMessages = ChatMessage::where('username', $currentUser->username)->get();
        $zipArchive->addFromString('chat-messages.txt', $chatMessages->toJson());

        $zipArchive->close();

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=personal-info.zip');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);

        exit;
    }

    /**
     * Instantiates profile information validator.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function profileValidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191',
        ]);
    }

    /**
     * Instantiates change password validator.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function changePasswordValidator(array $data)
    {
        return Validator::make($data, [
            'old_password' => 'required|string|min:6',
            'new_password' => 'required|string|min:6|confirmed'
        ]);
    }

    /**
     * Generates profile picture validator.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function profilePictureValidator(array $data)
    {
        return Validator::make($data, [
            'avatar' => 'required|image|max:4096'
        ]);
    }

    /**
     * Generates additional info validator.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function additionalInfoValidator(array $data)
    {
        return Validator::make($data, [
            'additional_info_address' => 'nullable|string',
            'additional_info_phone_number'  => ['nullable', 'regex:/^((\+381)|(0))(\s|-)?6(([0-6]|[8-9])(\s|-)?\d{7}|(77|78)(\s|-)?\d{6})$/'],
            'additional_info_occupation' => 'nullable|string|max:64',
        ]);
    }
}
