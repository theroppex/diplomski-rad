<?php

namespace App\Http\Controllers;

use App\Post;
use App\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('topic');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topic::all();

        $courseIds = Auth::user()->followedCourses->pluck('id');
        $postIds = Auth::user()->completedPosts->pluck('id');

        $posts = Post::whereIn('course_id', $courseIds)
            ->whereNotIn('id', $postIds)
            ->orderBy('id', 'desc')
            ->paginate(10);

        return view('home', ['topics' => $topics, 'posts' => $posts]);
    }
}
