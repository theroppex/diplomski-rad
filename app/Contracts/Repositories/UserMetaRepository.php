<?php

namespace App\Contracts\Repositories;

use App\Objects\UserMetaData;
use App\User;

interface UserMetaRepository
{
    /**
     * Retrieves user metadata.
     *
     * @param User $user
     *
     * @return UserMetaData
     */
    public function getUserMetaData(User $user): UserMetaData;

    /**
     * Sets user metadata.
     *
     * @param User $user
     * @param array $data
     *
     * @return UserMetaData
     */
    public function setUserMetaData(User $user, array $data): UserMetaData;
}