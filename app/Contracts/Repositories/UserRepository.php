<?php
/**
 * Author: Petar Rankovic
 * Date: 2/24/2019
 * Time: 10:19 AM
 */

namespace App\Contracts\Repositories;


use App\User;
use Illuminate\Database\Eloquent\Collection;

interface UserRepository
{
    /**
     * Retrieves list of valid recipients
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @return Collection
     */
    public function getRecipients( \Illuminate\Contracts\Auth\Authenticatable $user);
}