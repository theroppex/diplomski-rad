<?php
/**
 * Author: Petar Rankovic
 * Date: 11/4/2018
 * Time: 3:13 PM
 */

namespace App\Authorization\Gates;


use App\Comment;
use App\Contracts\Repositories\UserMetaRepository;
use App\Course;
use App\NewsPost;
use App\Post;
use App\User;

class UserGate
{
    /**
     * Checks whether user can see profile page for editing user information.
     *
     * @param User $user
     * @param User $target
     *
     * @return bool
     */
    public function canViewEditUserProfilePage($user, $target): bool
    {
        return $user->id === $target->id;
    }

    /**
     * Checks whether user can edit profile information.
     *
     * @param User $user
     * @param User $target
     *
     * @return bool
     */
    public function canEditUserProfile($user, $target): bool
    {
        return $user->id === $target->id;
    }

    /**
     * Checks whether user can change password.
     *
     * @param User $user
     * @param User $target
     *
     * @return bool
     */
    public function canChangePassword($user, $target): bool
    {
        return $user->id === $target->id;
    }

    /**
     * Checks whether user can change profile picture.
     *
     * @param User $user
     * @param User $target
     *
     * @return bool
     */
    public function canChangeProfilePicture($user, $target): bool
    {
        return $user->id === $target->id;
    }

    /**
     * Checks whether user can block other user.
     *
     * @param User $user
     * @param User $target
     * @return bool
     */
    public function canBlockUser($user, $target): bool
    {
        $isAlreadyBlocked = $user->blockedUsers()->where('blockee', $target->id)->exists();

        return !$isAlreadyBlocked && $user->id !== $target->id;
    }

    /**
     * Checks whether user can block other user.
     *
     * @param User $user
     * @param User $target
     * @return bool
     */
    public function canUnblockUser($user, $target): bool
    {
        return $user->blockedUsers()->where('blockee', $target->id)->exists();
    }

    /**
     * Checks whether user can follow course.
     *
     * @param User $user
     * @param Course $target
     * @return bool
     */
    public function canFollowCourse($user, $target): bool
    {
        return !($user->followedCourses()->where('course_id', $target->id)->exists());
    }

    /**
     * Checks whether user can unfollow course.
     *
     * @param User $user
     * @param Course $target
     * @return bool
     */
    public function canUnfollowCourse($user, $target): bool
    {
        return !$this->canFollowCourse($user, $target);
    }

    /**
     * Checks whether user can follow other user.
     *
     * @param User $user
     * @param User $target
     * @return bool
     */
    public function canFollowUser($user, $target): bool
    {
        $isAlreadyBlocked = $user->followedUsers()->where('followed', $target->id)->exists();

        return !$isAlreadyBlocked && $user->id !== $target->id;
    }

    /**
     * Checks whether user can unfollow other user.
     *
     * @param User $user
     * @param User $target
     * @return bool
     */
    public function canUnfollowUser($user, $target): bool
    {
        return $user->followedUsers()->where('followed', $target->id)->exists();
    }

    /**
     * Checks whether user can access chat or not.
     *
     * @param User $user
     * @return bool
     */
    public function canAccessChat($user): bool
    {
        /** @var UserMetaRepository $repository */
        $repository = app(UserMetaRepository::class);
        $metaData = $repository->getUserMetaData($user);

        return !$metaData->isChatBlocked();
    }

    /**
     * Checks whether user can delete news post.
     *
     * @param User $user
     * @param NewsPost $post
     * @return bool
     */
    public function canDeleteNewsPost($user, $post): bool
    {
        return $user->id === $post->author()->id;
    }

    /**
     * Checks whether user can delete comment or not.
     *
     * @param User $user
     * @param Comment $comment
     *
     * @return bool
     */
    public function canDeleteComment(User $user, Comment $comment): bool
    {
        return in_array($user->id, [$comment->user->id, $comment->post->user->id]);
    }

    /**
     * Checks whether user can delete post.
     *
     * @param User $user
     * @param Post $post
     *
     * @return bool
     */
    public function canDeletePost(User $user, Post $post): bool
    {
        return $user->id === $post->user->id;
    }

    /**
     * Checks whether user can delete post.
     *
     * @param User $user
     * @param Post $post
     *
     * @return bool
     */
    public function canDeleteCourse(User $user, Course $course): bool
    {
        return $user->id === $course->user->id;
    }

    /**
     * Checks whether user can mark post as completed.
     *
     * @param User $user
     * @param Post $target
     *
     * @return bool
     */
    public function canCompletePost(User $user, Post $target): bool
    {
        return !($user->completedPosts()->where('post_id', $target->id)->exists());
    }
}