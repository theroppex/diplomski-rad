<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ChatMessage
 * @package App
 *
 * @property string message
 * @property string username
 */
class ChatMessage extends Model
{
    /**
     * @inheritdoc
     */
    protected $fillable = [
        'message',
        'username',
    ];
}
