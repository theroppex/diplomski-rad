<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Course extends Model
{
    use Searchable;

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'title',
        'description',
        'topic_id',
        'user_id',
        'created_at',
        'updated_at',
    ];

    /**
     * Retrieves course author.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Retrieves course topic.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * @inheritdoc
     */
    public function toSearchableArray(): array
    {
        $searchData = [
            'title' => $this->title,
            'description' => $this->description
        ];

        return $searchData;
    }
}
