<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsPost extends Model
{
    /**
     * @inheritdoc
     */
    protected $fillable = [
        'title',
        'content'
    ];

    /**
     * @return User
     */
    public function author()
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->hasOne(User::class, 'id', 'author')->first();
    }
}
