<?php

namespace App\Repositories;

use App\Contracts\Repositories\UserMetaRepository as BaseRepository;
use App\Objects\UserMetaData;
use App\User;
use Illuminate\Support\Facades\DB;


class UserMetaRepository implements BaseRepository
{
    const TABLE = 'users_meta';

    const ALLOWED_KEYS = [
        'additional_info_address',
        'additional_info_occupation',
        'additional_info_phone_number',
    ];

    /**
     * Retrieves user metadata.
     *
     * @param User $user
     *
     * @return UserMetaData
     */
    public function getUserMetaData(User $user): UserMetaData
    {
        $metaData = DB::table(self::TABLE)
            ->where('user_id', $user->id)
            ->get(['meta_key', 'meta_value'])
            ->pluck('meta_value', 'meta_key')
            ->toArray();

        $result = new UserMetaData();

        foreach ($metaData as $key => $value) {
            if (in_array($key, self::ALLOWED_KEYS)) {
                $result->{$key} = $value;
            }
        }

        return $result;
    }

    /**
     * Sets user metadata.
     *
     * @param User $user
     * @param array $data
     *
     * @return UserMetaData
     */
    public function setUserMetaData(User $user, array $data): UserMetaData
    {
        $toBeCreated = array_filter($data, function ($item) {
            return !empty($item);
        });

        $toBeDeleted = array_map(function ($key, $value) {
            if (empty($value)) {
                return $key;
            }
        }, array_keys($data), $data);

        foreach ($toBeCreated as $key => $value) {
            DB::table(self::TABLE)
                ->updateOrInsert(
                    ['user_id' => $user->id, 'meta_key' => $key],
                    ['meta_value' => $value]
                );
        }

        DB::table(self::TABLE)
            ->where('user_id', $user->id)
            ->whereIn('meta_key', $toBeDeleted)
            ->delete();

        return $this->getUserMetaData($user);
    }
}