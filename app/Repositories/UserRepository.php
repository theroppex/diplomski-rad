<?php
/**
 * Author: Petar Rankovic
 * Date: 2/24/2019
 * Time: 10:20 AM
 */

namespace App\Repositories;

use App\Contracts\Repositories\UserRepository as BaseRepository;
use App\User;
use Illuminate\Database\Eloquent\Collection;

class UserRepository implements BaseRepository
{

    /**
     * Retrieves list of valid recipients
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @return Collection
     */
    public function getRecipients(\Illuminate\Contracts\Auth\Authenticatable $user)
    {
        $followerIds = $user->followedUsers()->pluck('followed');

        if ($followerIds->isEmpty()) {
            return Collection::make();
        }

        $blockedUsers = $user->blockedUsers()->pluck('blockee');

        $query = User::whereIn('id', $followerIds);

        if ($blockedUsers->isNotEmpty()) {
            $query->whereNotIn('id', $blockedUsers);
        }

        return $query->get();
    }
}