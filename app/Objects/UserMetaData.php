<?php
/**
 * Author: Petar Rankovic
 * Date: 11/12/2018
 * Time: 8:34 PM
 */

namespace App\Objects;

/**
 * Class UserMetaData
 * @package App\Objects
 */
class UserMetaData
{
    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->additional_info_address ?? '';
    }

    /**
     * @return string
     */
    public function getOccupation(): string
    {
        return $this->additional_info_occupation ?? '';
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->additional_info_phone_number ?? '';
    }

    /**
     * @return bool
     */
    public function isChatBlocked(): bool
    {
        return !empty($this->is_chat_blocked);
    }
}