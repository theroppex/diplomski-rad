<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    /**
     * @inheritdoc
     */
    protected $fillable = [
        'title',
    ];

    public function courses()
    {
        return $this->hasMany(Course::class);
    }
}
