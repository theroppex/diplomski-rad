<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Scout\Searchable;

/**
 * Class User
 * @package App
 *
 * @property int id
 * @property string name
 * @property string last_name
 * @property string email
 * @property string username
 * @property string password
 * @property string avatar_url
 * @property int favourite_topic
 */
class User extends Authenticatable
{
    use Notifiable;
    use Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'email', 'username', 'password', 'avatar_url', 'favourite_topic',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Retrieves avatar url.
     *
     * @return string
     */
    public function getAvatarAttribute(): string
    {
        if ($this->avatar_url === null) {
            $avatar = md5(trim($this->email));
            return "https://www.gravatar.com/avatar/$avatar?s=300&d=identicon";
        }

        return asset($this->avatar_url);
    }

    /**
     * Retrieves list of blocked users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function blockedUsers()
    {
        return $this->belongsToMany(
            'App\User',
            'user_blocked',
            'blocker',
            'blockee'
        );
    }

    /**
     * Retrieves list of followed users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followedUsers()
    {
        return $this->belongsToMany(
            'App\User',
            'user_followed',
            'follower',
            'followed'
        );
    }

    /**
     * Retrieves list of followed courses.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followedCourses()
    {
        return $this->belongsToMany(
            'App\Course',
            'courses_followed',
            'user_id',
            'course_id'
        );
    }

    /**
     * Retrieves list of completed posts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function completedPosts()
    {
        return $this->belongsToMany(
            'App\Post',
            'completed_posts',
            'user_id',
            'post_id'
        );
    }

    /**
     * Retrieves list of followers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followers()
    {
        return $this->belongsToMany(
            'App\User',
            'user_followed',
            'followed',
            'follower'
        );
    }

    /**
     * Retrieves list of news posts authored by this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function newsPosts()
    {
        return $this->hasMany('App\NewsPost', 'author', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * @inheritdoc
     */
    public function searchableAs(): string
    {
        return 'users_index';
    }

    /**
     * @inheritdoc
     */
    public function toSearchableArray(): array
    {
        $searchData = [
            'name' => $this->name,
            'last_name' => $this->last_name,
            'username' => $this->username,
            'email' => $this->email,
        ];

        return $searchData;
    }
}
