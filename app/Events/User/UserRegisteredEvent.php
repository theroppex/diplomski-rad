<?php
/**
 * Author: Petar Rankovic
 * Date: 10/7/2018
 * Time: 5:53 PM
 */

namespace App\Events\User;


use App\User;
use Illuminate\Queue\SerializesModels;

class UserRegisteredEvent
{
    use SerializesModels;

    /**
     * @var User
     */
    protected $user;
    /**
     * Unix timestamp.
     *
     * @var int
     */
    protected $registeredAt;

    /**
     * UserRegisteredEvent constructor.
     * @param User $user
     * @param int $registeredAt
     */
    public function __construct(User $user, int $registeredAt)
    {
        $this->user = $user;
        $this->registeredAt = $registeredAt;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getRegisteredAt(): int
    {
        return $this->registeredAt;
    }
}