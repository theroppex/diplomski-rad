<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PrivateMessages
 * @package App
 *
 * @property int $user_id
 * @property int $recipient_id
 * @property string $message
 */
class PrivateMessages extends Model
{
    protected $fillable = [
        'user_id',
        'recipient_id',
        'message',
    ];
}
