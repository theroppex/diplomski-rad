<nav class="navbar navbar-expand-md navbar-light navbar-laravel fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/home') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                @auth
                    <li class="nav-item">
                        <a href="/courses" class="nav-link">Courses</a>
                    </li>
                    <li class="nav-item">
                        <a href="/users" class="nav-link">Users</a>
                    </li>
                    <li class="nav-item">
                        <a href="/chat" class="nav-link">Chat</a>
                    </li>
                    <li class="nav-item">
                        <a href="/news" class="nav-link">News</a>
                    </li>
                @endauth
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link text-info" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    <li class="nav-item">
                        @if (Route::has('register'))
                            <a class="nav-link text-success" href="{{ route('register') }}">{{ __('Register') }}</a>
                        @endif
                    </li>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <img class="rounded profile-image" src="{{Auth::User()->avatar}}" alt="Avatar">{{ Auth::user()->username }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right border-info" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ '/users/' . Auth::user()->id . '/profile' }}">
                                <div class="row">
                                    <div class="col-md-6">
                                        {{ __('Profile') }}
                                    </div>
                                    <div class="col-md-6">
                                         <span class="float-md-right">
                                            <i class="fas fa-user"></i>
                                        </span>
                                    </div>
                                </div>
                            </a>

                            <a class="dropdown-item" href="/messages">
                                <div class="row">
                                    <div class="col-md-6">
                                        {{ __('Messages') }}
                                    </div>
                                    <div class="col-md-6">
                                         <span class="float-md-right">
                                            <i class="fas fa-envelope"></i>
                                        </span>
                                    </div>
                                </div>
                            </a>

                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <div class="row">
                                    <div class="col-md-6">
                                        {{ __('Logout') }}
                                    </div>
                                    <div class="col-md-6">
                                         <span class="float-md-right">
                                            <i class="fas fa-sign-out-alt"></i>
                                        </span>
                                    </div>
                                </div>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>