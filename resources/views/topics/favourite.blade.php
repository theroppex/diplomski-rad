@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>Please choose your favourite topic:</h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form action="/topic/favourite" method="post">
                    {{@csrf_field()}}
                    <div class="form-group">
                        <select class="form-control" id="topic" name="topic" title="Topic">
                            @foreach($topics as $topic)
                                <option value="{{$topic->id}}">{{$topic->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-outline-primary">
                            Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop