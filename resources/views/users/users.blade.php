@extends('layouts.app')

@section('includes')
    <script src="{{ asset('js/users.js') }}"></script>
@stop

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="search-icon"><i class="fas fa-search"></i></span>
                            </div>
                            <input type="text" class="form-control" id="userSearchBox" placeholder="Search..." aria-label="Search" aria-describedby="search-icon">
                        </div>
                    </div>
                </div>

                <div class="row" id="users">
                    @include('users.snippets.users')
                </div>
            </div>
        </div>
    </div>
@stop