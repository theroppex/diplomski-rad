@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <img class="img-fluid rounded-borders" src="{{Auth::User()->avatar}}" alt="Profile Image">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-block btn-outline-primary spaced-btn-group" data-toggle="modal" data-target="#changeImageModal">Change Picture</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="/users/{{Auth::User()->id}}/password">
                                    <button class="btn btn-block btn-outline-primary spaced-btn-group">Change Password</button>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="/users/personalinfo" download value="">
                                    <button class="btn btn-block btn-outline-primary spaced-btn-group">Download Personal Information</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                @include('flash.message')

                                <div class="card border-info">
                                    <div class="card-header border-info bg-white text-info">
                                        Essential Information
                                    </div>
                                    <div class="card-body">

                                        <div class="form-group row">
                                            <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                                            <div class="col-md-6">
                                                <input id="username" type="text" class="form-control" name="username" value="{{ $user->username }}"  disabled>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                            <div class="col-md-6">
                                                <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" disabled>
                                            </div>
                                        </div>

                                        <form method="POST" action="{{ "/users/$user->id/profile"}}">
                                            {{ method_field('PUT') }}

                                            @csrf
                                            <div class="form-group row">
                                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                                <div class="col-md-6">
                                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user->name }}" required autofocus>

                                                    @if ($errors->has('name'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                                                <div class="col-md-6">
                                                    <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ $user->last_name }}" required autofocus>

                                                    @if ($errors->has('last_name'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('last_name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group row mb-0">
                                                <div class="col-md-6 offset-md-4">
                                                    <button type="submit" class="btn btn-outline-primary btn-block">
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-top: 20px">
                            <div class="col-md-12">
                                <div class="card border-info">
                                    <div class="card-header border-info bg-white text-info">
                                        Additional Information
                                    </div>
                                    <div class="card-body">
                                        <form method="POST" action="{{ "/users/$user->id/additional_info"}}">

                                            @csrf
                                            <div class="form-group row">
                                                <label for="additional_info_address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                                                <div class="col-md-6">
                                                    <input id="additional_info_address" type="text" class="form-control{{ $errors->has('additional_info_address') ? ' is-invalid' : '' }}" name="additional_info_address" value="{{ $meta->getAddress() }}"  autofocus>

                                                    @if ($errors->has('additional_info_address'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('additional_info_address') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="additional_info_phone_number" class="col-md-4 col-form-label text-md-right">{{ __('Mobile Phone') }}</label>

                                                <div class="col-md-6">
                                                    <input id="additional_info_phone_number" type="text" class="form-control{{ $errors->has('additional_info_phone_number') ? ' is-invalid' : '' }}" name="additional_info_phone_number" value="{{ $meta->getPhoneNumber() }}"  autofocus>

                                                    @if ($errors->has('additional_info_phone_number'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('additional_info_phone_number') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="additional_info_occupation" class="col-md-4 col-form-label text-md-right">{{ __('Occupation') }}</label>

                                                <div class="col-md-6">
                                                    <input id="additional_info_occupation" type="text" class="form-control{{ $errors->has('additional_info_occupation') ? ' is-invalid' : '' }}" name="additional_info_occupation" value="{{ $meta->getOccupation() }}"  autofocus>

                                                    @if ($errors->has('additional_info_occupation'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('additional_info_occupation') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group row mb-0">
                                                <div class="col-md-6 offset-md-4">
                                                    <button type="submit" class="btn btn-outline-primary btn-block">
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row" style="margin-top: 20px">
                            <div class="col-md-12">
                                <div class="card border-info">
                                    <div class="card-header border-info bg-white text-info">
                                        Favourite topic
                                    </div>
                                    <div class="card-body">
                                        <form method="POST" action="/topic/favourite">
                                            @csrf

                                            <input type="hidden" name="fromProfile" id="fromProfile" value="1">

                                            <div class="form-group row">
                                                <label for="additional_info_occupation" class="col-md-4 col-form-label text-md-right">{{ __('Available topics') }}</label>

                                                <div class="col-md-6">
                                                    <select class="form-control" id="topic" name="topic" title="Topic">
                                                        @foreach($topics as $topic)
                                                            <option value="{{$topic->id}}" @if($user->favourite_topic === $topic->id) selected @endif>{{$topic->title}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-0">
                                                <div class="col-md-6 offset-md-4">
                                                    <button type="submit" class="btn btn-outline-primary btn-block">
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-top: 20px">
                            <div class="col-md-12">

                                <div id="accordion">
                                    <div class="card border-info">
                                        <div class="card-header border-info bg-white text-info" id="newsCollapseHeading">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link" data-toggle="collapse" data-target="#newsCollapse" aria-controls="newsCollapse">
                                                    News Posts
                                                </button>
                                            </h5>
                                        </div>

                                        <div id="newsCollapse" class="collapse show" aria-labelledby="newsCollapseHeading" data-parent="#accordion">
                                            <div class="card-body">
                                                @if(count($news) === 0)
                                                    You have no news posts.
                                                @else
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">#</th>
                                                            <th scope="col">Title</th>
                                                            <th scope="col">Created</th>
                                                            <th scope="col">Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($news as $index => $newsPost)
                                                            <tr>
                                                                <th scope="row">{{$index + 1}}</th>
                                                                <td>{{$newsPost->title}}</td>
                                                                <td>{{$newsPost->created_at->diffForHumans()}}</td>
                                                                <td>
                                                                    <a href="/news/{{$newsPost->id}}/delete" class="text-secondary">
                                                                        <i class="far fa-trash-alt delete-post"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                    <div class="row justify-content-center">
                                                        <div class="col-md-3 justify-content-center" style="display: flex">
                                                            <div style="margin: 0 auto">
                                                                {{$news->links()}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Profile Image Change Modal -->
    <div class="modal fade" id="changeImageModal" tabindex="-1" role="dialog" aria-labelledby="changeImageModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="changeImageModalLabel">Change Picture</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/users/{{Auth::User()->id}}/avatar" method="post" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="avatar" class="col-md-4 col-form-label text-md-right">{{ __('New Picture ') }}</label>

                            <div class="col-md-6" style="padding-top: 6px">
                                <input id="avatar" type="file" class="form-control-file" name="avatar" required accept="image/*">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-outline-primary btn-block">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop