@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <img class="img-fluid rounded-borders" src="{{Auth::User()->avatar}}" alt="Profile Image">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="/users/{{Auth::User()->id}}/profile">
                                    <button class="btn btn-block btn-outline-primary spaced-btn-group">
                                        <i class="fas fa-long-arrow-alt-left"></i> Back To Profile
                                    </button>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-block btn-outline-primary spaced-btn-group" data-toggle="modal" data-target="#changeImageModal">Change Picture</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-block btn-outline-primary spaced-btn-group">Download Personal Information</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                @include('flash.message')
                                <div class="card border-info">
                                    <div class="card-header border-info bg-white text-info">
                                        Change Password
                                    </div>
                                    <div class="card-body">
                                        <form action="/users/{{Auth::User()->id}}/password" method="post">
                                            @csrf

                                            <div class="form-group row">
                                                <label for="old_password" class="col-md-4 col-form-label text-md-right">{{ __('Old Password') }}</label>

                                                <div class="col-md-6">
                                                    <input id="old_password" type="password" class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}" name="old_password" required>

                                                    @if ($errors->has('old_password'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('old_password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="new_password" class="col-md-4 col-form-label text-md-right">{{ __('New Password') }}</label>

                                                <div class="col-md-6">
                                                    <input id="new_password" type="password" class="form-control{{ $errors->has('new_password') ? ' is-invalid' : '' }}" name="new_password" required>

                                                    @if ($errors->has('new_password'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('new_password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="new_password_confirmation" class="col-md-4 col-form-label text-md-right">{{ __('confirmation') }}</label>

                                                <div class="col-md-6">
                                                    <input id="new_password_confirmation" type="password" class="form-control{{ $errors->has('new_password_confirmation') ? ' is-invalid' : '' }}" name="new_password_confirmation" required>

                                                    @if ($errors->has('new_password_confirmation'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('new_password_confirmation') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group row mb-0">
                                                <div class="col-md-6 offset-md-4">
                                                    <button type="submit" class="btn btn-outline-primary btn-block">
                                                        Change
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Profile Image Change Modal -->
    <div class="modal fade" id="changeImageModal" tabindex="-1" role="dialog" aria-labelledby="changeImageModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="changeImageModalLabel">Change Picture</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/users/{{Auth::User()->id}}/avatar" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="avatar" class="col-md-4 col-form-label text-md-right">{{ __('New Picture ') }}</label>

                            <div class="col-md-6">
                                <input id="avatar" type="file" class="form-control" name="avatar" required accept="image/*">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-outline-primary btn-block">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop