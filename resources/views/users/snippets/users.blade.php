<?php
    /** @var \App\Contracts\Repositories\UserMetaRepository $repository */
    $repository = app(\App\Contracts\Repositories\UserMetaRepository::class);

    function getOccupation($occupation) {
        return !empty($occupation) ? $occupation : 'Enthusiast';
    }
?>

@foreach($users as $user)
    <div class="col-md-4">
        <div class="card user-card">
            <img class="card-img-top" src="{{$user->avatar}}" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">{{$user->name}}</h5>
                <p class="card-text">{{getOccupation($repository->getUserMetaData($user)->getOccupation())}}, <b>{{$user->courses()->count()}}</b> courses</p>
                @if(Gate::allows('view_edit_user_profile_page', $user))
                    <a href="/users/{{$user->id}}/profile" class="btn btn-block btn-outline-primary">Profile</a>
                @endif
                @if(Gate::allows('can_follow_user', $user))
                    <a href="/users/{{$user->id}}/follow" class="btn btn-block btn-outline-primary">Follow</a>
                @endif
                @if(Gate::allows('can_unfollow_user', $user))
                    <a href="/users/{{$user->id}}/unfollow" class="btn btn-block btn-outline-danger">Unfollow</a>
                @endif
                @if(Gate::allows('can_block_user', $user))
                    <a href="/users/{{$user->id}}/block" class="btn btn-block btn-outline-danger">Block</a>
                @endif
                @if(Gate::allows('can_unblock_user', $user))
                    <a href="/users/{{$user->id}}/unblock" class="btn btn-block btn-outline-primary">Unblock</a>
                @endif
            </div>
        </div>
    </div>
@endforeach