@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
                @foreach($topics as $topic)
                    <div class="col-md-6">
                        <div class="topic-card">
                            <div class="topic-gradient">

                            </div>
                            <div class="topic-title">
                                {{$topic->title}}
                            </div>
                            <div class="topic-selector">
                                <a href="/courses/topic/{{$topic->id}}"><i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    @if($posts->isNotEmpty())
        <div class="row justify-content-center">
            <h3>Don't miss these posts.</h3>
        </div>
    @else
        <div class="row justify-content-center">
            <h3>Posts will appear here when you follow some courses.</h3>
        </div>
    @endif


    <div class="row justify-content-center" style="padding: 0 20px 0 20px">
        <div class="col-md-12">
            @foreach($posts as $post)
                <div class="row justify-content-center" style="margin-top: 20px">
                    <div class="col-md-7 course-title">
                        <a href="/posts/{{$post->type}}/{{$post->id}}"><h4>{{$post->title}}</h4></a>
                    </div>
                    <div class="col-md-1 justify-content-center course-info">
                            <span class="badge badge-pill badge-danger text-white" style="margin-left: 12px;">
                                <span class="badge badge-pill badge-danger text-white">
                                    {{$post->type}}
                                </span>
                            </span>

                        @if(Gate::allows('can_delete_post', $post))
                            <div class="comment-delete">
                                <a href="/posts/delete/{{$post->id}}">
                                    <i class="fa fa-times-circle delete-message"></i>
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="row justify-content-center fixed-bottom">
        <div class="col-md-4 justify-content-center" style="display: flex">
            <div style="margin: 0 auto">
                {{$posts->links()}}
            </div>
        </div>
    </div>
</div>
@endsection
