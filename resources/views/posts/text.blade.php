@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    @include('flash.message')
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8 course-title-no-hover">
                <h3>{{$post->title}}</h3>
            </div>
        </div>

        <div class="row justify-content-center" style="margin-top: 20px">
            <div class="col-md-8 course-title-no-hover description">
                {{$post->description}}
            </div>
        </div>

        <div class="row justify-content-center" style="margin-top: 40px">
            <div class="col-md-8 course-info">
                {!! $post->data !!}
            </div>
        </div>

        @foreach($post->comments as $comment)
            <div class="row justify-content-center" style="margin-top: 20px">
                <div class="col-md-6 course-info" style="background-color: #fff7f7;">
                    <h5>{{$comment->comment}}</h5>
                </div>
                <div class="col-md-2 course-info comment-info" style="background-color: #fff7f7;">
                    <h6>{{$comment->user->username}}, <br/> {{$comment->created_at->diffForHumans()}}</h6>

                    @if(Gate::allows('can_delete_comment', $comment))
                        <div class="comment-delete">
                            <a href="/comments/delete/{{$comment->id}}">
                                <i class="fa fa-times-circle delete-message"></i>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        @endforeach

        <div class="row justify-content-center" style="padding: 0; margin-top: 40px;">
            <div class="col-md-8" style="padding: 0">
                <form action="/posts/{{$post->id}}/comments" method="post">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <textarea class="form-control" name="body" id="body" cols="30" rows="5" required>
                                {{old('body')}}
                        </textarea>
                    </div>
                    <button type="submit" class="btn btn-block btn-outline-primary spaced-btn-group">Submit Comment</button>
                </form>
            </div>
        </div>
    </div>

    @include('posts.snips.side')
@stop