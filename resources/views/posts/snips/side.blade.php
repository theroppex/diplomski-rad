<div class="side-action">
    <a href="/courses/{{$post->course->id}}" title="Back">
        <div class="side-action-button back">
            <i class="fas fa-chevron-left"></i>
        </div>
    </a>

    @if(Gate::allows('can_complete_post', $post))
        <a href="/posts/{{$post->id}}/complete" title="Mark as completed.">
            <div class="side-action-button not-complete">
                <i class="fas fa-check"></i>
            </div>
        </a>
    @else
        <div class="side-action-button complete">
            <i class="fas fa-check"></i>
        </div>
    @endif

    @if(Gate::allows('can_delete_post', $post))
        <a href="/posts/delete/{{$post->id}}" title="Delete">
            <div class="side-action-button delete">
                <i class="fas fa-trash"></i>
            </div>
        </a>
    @endif
</div>