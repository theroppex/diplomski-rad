@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('flash.message')
            </div>
        </div>
    </div>


    @include('courses.snippets.compose')

   <div class="container">
       <form action="/courses/search" method="post">
            <div class="row justify-content-center">
                <div class="col-md-8" style="padding: 0;">
                    {!! csrf_field() !!}
                    <div class="input-group">
                        <input type="text" class="form-control" id="search" name="search" placeholder="Search..." aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-outline-primary" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
       </form>

       @include('courses.snippets.course')
   </div>

    <div class="row justify-content-center fixed-bottom">
        <div class="col-md-4 justify-content-center" style="display: flex">
            <div style="margin: 0 auto">
                {{$courses->links()}}
            </div>
        </div>
    </div>
@stop