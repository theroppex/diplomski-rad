@extends('layouts.app')

<script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=ws9mzjhfss5x9e6ghr45pisw0dggjkmvgxkmcqoe3em3h7vg"></script>
<script>tinymce.init({
        selector:'#body',
        height: '700px',
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount'
        ],
        toolbar: 'undo redo | insert | styleselect | fontsizeselect | fontselect | forecolor | backcolor | bold | italic | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | link | image',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tiny.cloud/css/codepen.min.css'
        ],
        font_formats: 'Lato=lato,arial,helvetica,sans-serif;Nunito=Nunito, sans-serif;Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;',
        fontsize_formats: '8px 10px 12px 14px 18px 24px 36px'
    });</script>

@section('content')
    <div class="container">
        @include('flash.message')
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form action="/courses/{{$course->id}}/create/text" method="post">
                    {{ @csrf_field() }}
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" placeholder="Intriguing title." class="form-control" id="title" name="title" maxlength="191" required value="{{old('title')}}">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" class="form-control" name="description" id="description" placeholder="Short description of your new text post." maxlength="191" required value="{{old('description')}}">
                    </div>
                    <textarea name="body" id="body">{{old('body')}}</textarea>
                    <button type="submit" class="btn btn-block btn-outline-primary spaced-btn-group">Submit</button>
                </form>
            </div>
        </div>
    </div>
@stop