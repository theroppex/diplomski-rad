@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center" style="padding: 0">
            <div class="col-md-8" style="padding: 0">
                @include('flash.message')
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8 course-title-no-hover">
                <h3>{{$course->title}}</h3>
            </div>
        </div>

        <div class="row justify-content-center" style="margin-top: 20px">
            <div class="col-md-8 course-title-no-hover description">
                {{$course->description}}
            </div>
        </div>

        @foreach($course->posts as $post)
            <div class="row justify-content-center" style="margin-top: 20px">
                <div class="col-md-7 course-title">
                    <a href="/posts/{{$post->type}}/{{$post->id}}"><h4>{{$post->title}}</h4></a>
                </div>
                <div class="col-md-1 justify-content-center course-info">
                    <span class="badge badge-pill badge-danger text-white" style="margin-left: 12px;">
                        <span class="badge badge-pill badge-danger text-white">
                            {{$post->type}}
                        </span>
                    </span>

                    @if(Gate::allows('can_delete_post', $post))
                        <div class="comment-delete">
                            <a href="/posts/delete/{{$post->id}}">
                                <i class="fa fa-times-circle delete-message"></i>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        @endforeach

    </div>

    <a class="compose-msg compose-text" href="/courses/{{$course->id}}/create/text">
        <i class="fa fa-file-medical">

        </i>
    </a>

    <a class="compose-msg compose-video" data-toggle="modal" data-target="#uploadVideoModal">
        <i class="fa fa-file-video">

        </i>
    </a>


    <!-- Profile Image Change Modal -->
    <div class="modal fade" id="uploadVideoModal" tabindex="-1" role="dialog" aria-labelledby="uploadVideoModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="uploadVideoModalLabel">Create Video Post</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/courses/{{$course->id}}/create/video" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="title">{{ __('Title') }}</label>
                                <input id="title" type="text" class="form-control" name="title" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="description">{{ __('Description') }}</label>
                                <input id="description" type="text" class="form-control" name="description" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="avatar" class="col-md-4 col-form-label text-md-right">{{ __('Your video') }}</label>

                            <div class="col-md-6" style="padding-top: 6px">
                                <input id="video" type="file" class="form-control-file" name="video" required accept="video/mp4">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-outline-primary btn-block">
                            Upload
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop