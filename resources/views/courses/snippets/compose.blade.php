<div class="compose-msg" data-toggle="modal" data-target="#composePost">
    <i class="fa fa-plus">

    </i>
</div>


<!-- Compose News Post Modal -->
<div class="modal fade" id="composePost" tabindex="-1" role="dialog" aria-labelledby="composePostLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="composePostLabel">Create new course</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/courses/" method="post">
                @csrf

                <div class="modal-body">

                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" maxlength="191" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" class="form-control" name="description" id="description" placeholder="Short description of your new course." maxlength="191" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Topic</label>
                        <select class="form-control" id="topic" name="topic" title="Topic" required>
                            @foreach($topics as $topic)
                                <option value="{{$topic->id}}" @if($user->favourite_topic === $topic->id) selected @endif>{{$topic->title}}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-primary btn-block">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>