@if($courses->isEmpty())
    <div class="row justify-content-center" style="margin-top: 10px">
        <h3>We are unable to find any courses.</h3>
    </div>
@endif

@foreach($courses as $course)
    <div class="row justify-content-center" style="margin-top: 20px">
        <div class="col-md-6 course-title">
            <a href="/courses/{{$course->id}}">
                <h3>{{$course->title}}</h3>
            </a>
        </div>
        <div class="col-md-2 course-info">
            <div class="row">
                <div class="col-md-4 course-language">
                    <span class="badge badge-pill badge-danger text-white">
                        {{$course->topic->title}}
                    </span>
                </div>
                <div class="col-md-4 cour course-lessons">
                    <h5 title="Lessons">{{$course->posts()->count()}}</h5>
                </div>
                <div class="col-md-4 course-follow">
                    @if (Gate::allows('can_follow_course', $course))
                        <a href="/courses/{{$course->id}}/follow">
                            <i class="fa fa-heart follow" title="Follow"></i>
                        </a>
                    @endif
                    @if (Gate::allows('can_unfollow_course', $course))
                        <a href="/courses/{{$course->id}}/unfollow">
                            <i class="fa fa-heart unfollow" title="Unfollow"></i>
                        </a>
                    @endif
                    @if(Gate::allows('can_delete_course', $course))
                        <div class="comment-delete">
                            <a href="/courses/delete/{{$course->id}}">
                                <i class="fa fa-times-circle delete-message"></i>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endforeach