@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card border-info">
                    <div class="card-header border-info bg-white text-info">
                        What's on your mind?
                    </div>
                    <div class="card-body">
                        <chat-log :messages="messages"></chat-log>
                        <chat-composer v-on:messagesent="addMessage" username="{{Auth::user()->username}}"></chat-composer>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop