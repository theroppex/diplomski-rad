@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        @include('messages.snippets.recipients')
                    </div>
                    <div class="col-md-6 offset-md-1">
                        @include('messages.snippets.messages')
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($selectedUserId)
        @include('messages.snippets.compose')
    @endif
@endsection