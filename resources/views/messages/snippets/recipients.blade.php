@foreach($recipients as $recipient)
    <div class="row message-recipient-row {{$recipient->id === $selectedUserId ? 'selected' : ''}}">
        <div class="col-md-3">
            <a href="/messages/{{$recipient->id}}">
                <img class="img-fluid rounded-circle" src="{{$recipient->avatar}}" alt="Profile pic">
            </a>
        </div>
        <div class="col-md-9 message-recipient-name">
            {{$recipient->name}}
        </div>
    </div>
@endforeach