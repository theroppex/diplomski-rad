<div class="compose-msg" data-toggle="modal" data-target="#composeMessage">
    <i class="fa fa-paper-plane">

    </i>
</div>

<div class="modal fade" id="composeMessage" tabindex="-1" role="dialog" aria-labelledby="composePostLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="composePostLabel">Compose message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/messages/{{$selectedUserId}}" method="post">
                @csrf

                <div class="modal-body">

                    <div class="form-group">
                        <label for="recipient">Recipient</label>
                        <input type="text" class="form-control" id="recipient" name="recipient" readonly value="{{$selectedUserName}}">
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <input type="text" class="form-control" name="message" id="message" maxlength="191">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-primary btn-block">
                        Send
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>