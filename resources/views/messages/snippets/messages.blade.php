@if($selectedUserId && $messages->count() === 0)
    <div class="row">
        <div class="col-md-12 text-center">
            <h5>
                There are no messages for this recipient.
            </h5>
        </div>
    </div>
@endif

@foreach($messages as $message)
    <div class="row">
        <div class="col-md-12 message-row {{$selectedUserId === $message->user_id ? 'sender' : 'owner'}}">
            <div class="row">
                <div class="col-md-11 message">
                    {{$message->message}}
                </div>
                <div class="col-md-1">
                    <a href="/messages/delete/{{$message->id}}">
                        <i class="fa fa-times-circle delete-message"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endforeach