<div class="container news-stack">
    <div class="row justify-content-center">
        <div class="col-md-9">
            @if(count($posts) === 0)
                <div class="col-md-12 text-center">
                    <h3>No news found.</h3>
                </div>
            @else
            <div class="row">
                @foreach($posts as $post)
                    <div class="col-md-4">
                        <div class="card news-card">
                            <div class="card-body">
                                <h5 class="card-title">{{$post->title}}</h5>
                                <p cl ass="card-text">{{$post->content}}</p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="/news/user/{{$post->author()->id}}"><small>{{$post->author()->username}}</small></a>
                                    </div>
                                    <div class="col-md-6 text-right text-secondary">
                                        @if(Gate::allows('can_delete_news_post', $post))
                                            <a href="/news/{{$post->id}}/delete" class="text-secondary">
                                                <i class="far fa-trash-alt delete-post"></i>
                                            </a>
                                        @else
                                            <small>
                                                {{$post->created_at->diffForHumans()}}
                                            </small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="row justify-content-center fixed-bottom">
                <div class="col-md-3 justify-content-center" style="display: flex">
                    <div style="margin: 0 auto">
                        {{$posts->links()}}
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>