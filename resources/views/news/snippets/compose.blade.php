<div class="container">
    <div class="row">
        <div class="col-md-10"></div>
        <div class="col-md-2">
            <button class="btn btn-block btn-outline-primary spaced-btn-group" data-toggle="modal" data-target="#composePost">Broadcast</button>
        </div>
    </div>
</div>


<!-- Compose News Post Modal -->
<div class="modal fade" id="composePost" tabindex="-1" role="dialog" aria-labelledby="composePostLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="composePostLabel">Update your followers</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="#" method="post">
                @csrf

                <div class="modal-body">

                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" maxlength="60">
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label>
                        <input type="text" class="form-control" name="content" id="content" placeholder="What's new?" maxlength="140">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-primary btn-block">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>