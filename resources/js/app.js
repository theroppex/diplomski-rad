
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('chat-message', require('./components/Chat/ChatMessage.vue'));
Vue.component('chat-log', require('./components/Chat/ChatLog.vue'));
Vue.component('chat-composer', require('./components/Chat/ChatComposer.vue'));

const app = new Vue({
    el: '#app',
    data: {
        messages: [],
    },
    methods: {
        addMessage(message) {
            axios.post('/chat/messages', message);
            $('html,body').animate({scrollTop: document.body.scrollHeight},"fast");
        }
    },
    created() {
        axios.get('/chat/messages').then(response => {
            if (response.data) {
                this.messages = response.data;
            }
        });

        Echo.private('chat_channel')
            .listen('MessageCreatedEvent', (e) => {
                if (e.message) {
                    this.messages.push(e.message);
                    $('html,body').animate({scrollTop: document.body.scrollHeight},"fast");
                }
            })
    }
});
