const MIN_QUERY_LENGTH = 2;
const USERS_FILTER_URL = '/users/filter/';

$(document).ready(
    function () {
        $("#userSearchBox").keyup(
            function () {
                let query = $('#userSearchBox').val();
                if (query && query.length >= MIN_QUERY_LENGTH) {
                    $.ajax(
                        {
                            type: 'GET',
                            url: USERS_FILTER_URL + '?q=' + query,
                            success: function (result) {
                                $('#users').html(result);
                            }
                        }
                    )
                } else {
                    $.ajax(
                        {
                            type: 'GET',
                            url: USERS_FILTER_URL,
                            success: function (result) {
                                $('#users').html(result);
                            }
                        }
                    )
                }
            }
        );
    }
);